const fs = require('fs');

try {
  let data = fs.readFileSync('./data_lexio.txt', 'utf8');

  const result = Array.from(new Set(data.split(''))).toString();
  console.log('First occurence from left to right:', result.replace(/,/g, ''));
} catch (err) {
  console.error(err);
}
