const fs = require('fs');

try {
  let data = fs.readFileSync('./data_value.txt', 'utf8');
  data = data.split(' ');
  data = data.map(val => +val);

  let minimumValue = data[0];
  let maximumProfit = tmpRange = 0
  let getValByIndex = 0;

  for (let i = 0; i < data.length; i++) {
    const value = data[i];
    if (minimumValue > value) minimumValue = value;
    
    tmpRange = value - minimumValue;
    if (tmpRange > maximumProfit) {
      maximumProfit = tmpRange;
      getValByIndex = i;
    }
  }

  console.log('minimumValue:', minimumValue);
  console.log('maximumValue:', data[getValByIndex]);
  console.log('maximumProfit:', maximumProfit);
} catch (err) {
  console.error(err);
}

